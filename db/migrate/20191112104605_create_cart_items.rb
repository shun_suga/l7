class CreateCartItems < ActiveRecord::Migration[5.2]
  def change
    create_table :cart_items do |t|
      t.integer :quantity
      t.references :product
      t.references :cart

      t.timestamps
    end
  end
end
